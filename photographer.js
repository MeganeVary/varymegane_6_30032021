/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
var params = new URLSearchParams(document.location.search.substring(1));
var pageId = parseInt(params.get("photographer-id"));
let profilContainer = document.getElementById("profil_photographe");
let sectionGalerie = document.getElementById("galerie");
let containerInformation = document.getElementById("informationRecap");
let photographer;
let likeActive = false;

var menu = document.querySelector("select");
menu.addEventListener("change", getOption);

fetch("FishEyeDataFR.json")
	.then((res) => res.json())
	.then((data) => {
		let dataPhotographer = data.photographers.find((el) => el.id === pageId);
		photographer = new Photographer(
			dataPhotographer.id,
			dataPhotographer.name,
			dataPhotographer.city,
			dataPhotographer.country,
			dataPhotographer.tags,
			dataPhotographer.tagline,
			dataPhotographer.price,
			dataPhotographer.portrait,
			(dataPhotographer.medias = [])
		);

		photographer.setMedias(
			data.media.filter((el) => el.photographerId === pageId)
		);

		//information profil
		formulaire.innerHTML = photographer.getModalProfil();

		profilContainer.innerHTML = photographer.getHTMLProfile();

		sectionGalerie.innerHTML = photographer.getHTMLGalleryByOptions();

		containerInformation.innerHTML = photographer.getHTMLInformationRecap();

		lightBox.init();
	});
function enterTest(e) {
	if (e.key == "Enter") {
		likesAdd(e.target);
	} else return;
}
function likesAdd(el) {
	console.log(el.dataset.active);
	if (el.dataset.active == "false") {
		let newLikes = parseInt(el.dataset.likes) + 1;
		el.innerHTML = newLikes;
		el.dataset.likes = newLikes;
		el.dataset.active = true;

		let likesPopUp = document.getElementById("test");
		let newLikesPopUp = parseInt(likesPopUp.dataset.likes) + 1;
		likesPopUp.textContent = newLikesPopUp;
		likesPopUp.dataset.likes = newLikesPopUp;
	} else {
		let newLikes = parseInt(el.dataset.likes) - 1;
		el.innerHTML = newLikes;
		el.dataset.likes = newLikes;
		el.dataset.active = false;

		let likesPopUp = document.getElementById("test");
		let newLikesPopUp = parseInt(likesPopUp.dataset.likes) - 1;
		likesPopUp.textContent = newLikesPopUp;
		likesPopUp.dataset.likes = newLikesPopUp;
	}
}
function getOption(e) {
	let option = e.target.value;
	let htmlGallery = photographer.getHTMLGalleryByOptions(option);
	sectionGalerie.innerHTML = htmlGallery;
	lightBox.init();
}