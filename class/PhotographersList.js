// eslint-disable-next-line no-unused-vars
class PhotographersList {

	constructor () {
		this.photographers = [];
	}

	pushPhotographer(photographer) {
		this.photographers.push(photographer);
	}
	getPhotographersByTagsBis(tags = []) {
		if (tags.length) {
			return this.photographers.filter((photographer) => {
				for (let tag of photographer.tags) {
					if (tags.includes(tag)) {
						return true;
					}
				}
				return false;
			});
		} else {
			return this.photographers;
		}
	}

}
