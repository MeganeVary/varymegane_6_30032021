/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const formatVideo = ".mp4";
const likesSort = "likes";
const dateSort = "date";

// MODAL //
let formulaire = document.getElementById("formulaire");
let modal = null;
const focusSelector = "button, a, text, input, textarea";
let elementsFocus = [];
let previousFocusElement = null;

const openModal = function () {
	formulaire.style.visibility = "visible";
	elementsFocus = Array.from(formulaire.querySelectorAll(focusSelector));
	previousFocusElement = document.querySelector(":focus");

	formulaire.setAttribute("aria-hidden", false);
	formulaire.setAttribute("aria-modal", true);
	modal = true;
};
function closeModal() {
	if (modal === null) {
		return;
	}
	if (previousFocusElement != null) {
		previousFocusElement.focus();
	}
	formulaire.style.visibility = "hidden";
	formulaire.setAttribute("aria-hidden", true);
	formulaire.setAttribute("aria-modal", false);
	modal = null;
}

const focusInModal = function (e) {
	e.preventDefault();
	let index = elementsFocus.findIndex(
		(f) => f === formulaire.querySelector(":focus")
	);
	console.log(e.key);
	if (e.shiftKey === true) {
		index--;
	} else {
		index++;
	}
	if (index >= elementsFocus.length) {
		index = 0;
	}
	if (index < 0) {
		index = elementsFocus.length - 1;
	}
	elementsFocus[index].focus();
	console.log(index);
};
const closeModalKey = window.addEventListener("keydown", function (e) {
	if (e.key === "Escape" || e.key === "Esc") {
		closeModal(e);
	}
	if (e.key === "Tab" && modal != null) {
		focusInModal(e);
	}
});

function formValide(form, e) {
	e.preventDefault();

	let formData = new DataForm(form, true);

	console.table(formData.data);
	if (!formData.isValid) {
		for (let error of formData.errors) {
			console.log(error);
		}

		for (let spanError of form.getElementsByClassName("spanErreur")) {
			spanError.textContent = "";
			spanError.style.display = "none";
		}

		document.querySelector(`input[name="${formData.errors[0].name}"]`).focus();

		formData.errors.forEach((error) => {
			let errorContainer = document.getElementById(error.name + "-error"); // span
			let ariaInput = document.getElementById(error.name);

			errorContainer.textContent = error.content;
			errorContainer.style.display = "block";

			ariaInput.setAttribute("aria-labelledby", error.name + "-error");
		});
	} else {
		window.location.href = window.location.href + "&message=sent";
	}

	return false;
}

// MODAL //

class Photographer {
	constructor(id, name, city, country, tags, tagline, price, portrait) {
		this.that = this;
		this.id = id;
		this.name = name;
		this.city = city;
		this.country = country;
		this.tags = tags;
		this.likes = 0;
		this.tagline = tagline;
		this.price = price;
		this.portrait = portrait;
		this.medias = [];
	}

	setMedias(mediasArray) {
		mediasArray.forEach((dataMedia) => {
			let mediaIsVideo = dataMedia.video != undefined;

			this.medias.push(
				MediaFactory.createMedia(
					dataMedia.id,
					dataMedia.photographerId,
					dataMedia.title,
					dataMedia.image ?? dataMedia.video,
					dataMedia.tags,
					dataMedia.likes,
					dataMedia.date,
					dataMedia.price,
					mediaIsVideo
				)
			);

			this.likes = this.likes + dataMedia.likes;
		});
	}

	getModalProfil() {
		return `          
        <button id="close" onclick="closeModal()" aria-label="fermer le formulaire"><img src="cross.svg" alt=""></button>
        <h1>Contactez-moi ${this.name}</h1>
        <fieldset id="formIsValid">
            <legend>Vos coordonnées</legend>

            <label for="prenom">Prénom</label> 
            <input type="text" name="prenom" id="prenom" aria-required="true" autocomplete="prenom"/>
            <span class="spanErreur" id="prenom-error"></span>
       
            <label for="nom">Nom</label>
            <input type="text" name="nom" id="nom" aria-required="true"  autocomplete="nom"/>
            <span class="spanErreur" id="nom-error"></span>

            <label for="email">E-mail</label>
            <input type="email" name="email" id="email" aria-required="true" autocomplete="email"/>
            <span class="spanErreur" id="email-error"></span>

            <legend>Votre message</legend>
            <label for="message">Votre message</label>
            <input type="text" name="message" id="message" aria-required="true" autocomplete="message"/>
            <span class="spanErreur" id="message-error"></span>

        </fieldset>

        <input type="submit" value="Envoyer" id="submit" >`;
	}
	getTags() {
		let tagHTML = "";
		this.tags.forEach((tag) => {
			tagHTML += ` <a class="button_tag" role="" aria-label="filtrer profil photographes par ${tag}" href="index.html?tag=${tag}" data-filter="${tag}" >#${tag}</a>`;
		});
		return tagHTML;
	}

	getHTMLCard() {
		return `
        <article class="profil_individuel_photographe" role="lien" aria-label="redirection profil individuel des photographes" id="Main">
            <h2>
                ${this.name}
            </h2>
            <a href="photographer.html?photographer-id=${this.id}" aria-label="profil de ${this.name}">
                <img class="photo_profil" src="fisheye_photos/Photographers ID Photos/${this.portrait}" alt="portrait photo du photographe ${this.name}">
            </a>
            <section class="information_photographe" role="information" aria-label="information du photographe ${this.name}" tabindex="0">
                <p id="localisation" role="localisation" aria-label="localisation du photographe" tabindex="0">
                    ${this.city},${this.country}
                </p>
                <p id="slogan" role="information" aria-label="slogan du photographe" tabindex="0">
                    ${this.tagline}
                </p>
                <p id="prix"tabindex="0" aria-label="prix du photographe" >
                    ${this.price}€/jour
                </p>
                <p class="containerTags" role="nav" aria-label="tags du photographe ${this.name}">
                    ${this.getTags()}
                </p>
            </section>
        </article>
        <div id="container"></div>
        `;
	}
	getHTMLProfile() {
		return `
        <h1>${this.name}</h1>
		<div id="containerButtonImg">	
		<img class="photo_profil" src="fisheye_photos/Photographers ID Photos/${this.portrait}" alt="portrait du photographe ${this.name}" tabindex="0">
        <button id="modalOpen" onclick="openModal()" role="modal" aria-label="formulaire de contact du photographe ${this.name}">Contactez moi</button>
		</div>
        <section id="information_photographe">
            <p id="localisation" role="localisation" aria-label="localisation du photographe" tabindex="0">
            ${this.city},${this.country}
            </p>
            <p id="slogan" role="information" aria-label="slogan du photographe" tabindex="0">
            ${this.tagline}
            </p>
            <p class="containerTags" role="nav" title="tags du photographe ${this.name}" aria-label="redirection vers la page accueil , filtré par le tag sélectionné"tabindex="0">
            ${this.getTags()}
            </p>
        </section>
        `;
	}
	getHTMLInformationRecap() {
		return ` 
        <p id="likesPopup" tabindex="1" aria-label="Nombre total de likes pour les photos du photographe ${this.name}" >
        <iaria-label="likes" class="fas fa-heart"></i>
        <span data-likes="${this.likes}" id="test">${this.likes}</span>
        </p>
        <p tabindex="1" aria-label="prix du photographe ${this.name}">${this.price}€/jour</p>
       `;
	}

	sortMedia(option) {
		var sortedMedias = this.medias.sort((media1, media2) => {
			if (media1[option] < media2[option]) {
				return -1;
			}
			if (media1[option] > media2[option]) {
				return 1;
			}
			return 0;
		});
		if (option === likesSort || option === dateSort) {
			sortedMedias.reverse();
		}
		return sortedMedias;
	}

	getHTMLGalleryByOptions(option = null) {
		var html = "";
		let sortedMedias = this.sortMedia(option);

		sortedMedias.forEach((media) => {
			if (media.url.includes(formatVideo)) {
				html += `<article>
                <a href="fisheye_photos/${this.name}/${media.url}"  title="${media.title}" >
                    <video aria-label="video" src="fisheye_photos/${this.name}/${media.url}" alt="${media.title}"
                        controls poster="fisheye_photos/${this.name}/${media.url}">
                    </video>
                </a>
                <section class="information_photo" role="information video" tabindex="0">
                    <p  tabindex="0" class="nom_photo" aria-label="nom de la photo">
                    ${media.title}
                    </p>
                    <p  tabindex="0" class="prix" aria-label="prix de la photo">
                    ${media.price}€
                    </p>
                    <p class="likes" aria-label="Nombre de likes de la photo" >
                    <i tabindex="0" aria-label="likes" class="fas fa-heart" data-active="false" data-likes="${media.likes}" onclick="likesAdd(this)">${media.likes}</i>
                    <p>
                </section>
            </article>`;
			} else {
				html += `<article>
                    <a href="fisheye_photos/${this.name}/${media.url}">
                        <img src="fisheye_photos/${this.name}/${media.url}" alt="${media.title}" aria-label="réalisation du photographe ${this.name}">
                    </a>
                    <section class="information_photo" role="information image" tabindex="0">
                        <p   tabindex="0" class="nom_photo" aria-label="nom de la photo">
                            ${media.title}
                        </p>
                        <p  tabindex="0" class="prix" aria-label="prix de la photo">
                        ${media.price}€
                        </p>
                        <p class="likes" aria-label="Nombre de likes de la photo" >
                            <i   tabindex="0" aria-label="likes" class="fas fa-heart"  data-active="false" data-likes="${media.likes}" onkeypress="enterTest(event); " onclick="likesAdd(this)">${media.likes}</i>
                        <p>
                    </section>
                </article>`;
			}
		});
		return html;
	}
}
window.addEventListener("keydown",function(e){
	console.log(e.key);
});