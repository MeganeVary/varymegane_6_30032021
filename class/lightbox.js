/* eslint-disable no-unused-vars */
class lightBox {
	static init() {
		const links = document.querySelectorAll(
			"a[href$= \".png\"],a[href$= \".jpg\"],a[href$= \".jpeg\"],a[href$= \".mp4\"]"
		);
		let gallery = [];

		for (let link of links) {
			gallery.push({
				url: link.getAttribute("href"),
				title: link.childNodes[1].getAttribute("alt"),
			});
		}

		links.forEach((link) =>
			link.addEventListener("click", (e) => {
				e.preventDefault();
				new lightBox(e.currentTarget.getAttribute("href"), gallery);
			})
		);
	}
	/*url = url de l'image gallery = chemins des images de la lightbox */
	constructor(url, gallery) {
		this.i = 0;

		let mediaObject = gallery.filter((media) => {
			if (media.url === url) {
				return media.title;
			}
		})[0];

		try {
			this.element = this.buildDOM();
		} catch (error) {
			console.error(error);
		} finally {
			this.gallery = gallery;
			this.oneKeyUpe = this.oneKeyUpe.bind(this);
			document.body.appendChild(this.element);
			document.addEventListener("keyup", this.oneKeyUpe);
			document.addEventListener("keydown", this.menuModal());
			this.loadImage(url, mediaObject.title);
		}
	}

	loadImage(url, title) {
		this.url = null;
		const image = new Image();
		const video = document.createElement("video");
		const mediaTitle = document.createElement("figcaption");
		const lightBox_bloc = this.element.querySelector(".lightBox_bloc");
		const loader = document.createElement("div");
		const divControls = document.createElement("div");
		const spaceKey = " ";

		divControls.setAttribute("id", "controls");
		divControls.innerHTML = `
        <button id="play">Play</button>
        <button id = "stop" > Stop</button >`;

		var boardControls = document.getElementById("Containercontrols");

		loader.classList.add("lightBox_loader");
		lightBox_bloc.innerHTML = "";
		lightBox_bloc.appendChild(loader);

		image.src = url;
		this.url = url;

		mediaTitle.setAttribute("id", "titrePhoto");
		mediaTitle.setAttribute("aria-label", "Titre du media");
		mediaTitle.textContent = title;

		if (url.includes(".mp4")) {
			video.src = url;
			video.autoplay = true;
			lightBox_bloc.appendChild(video);
			boardControls.style.display = "block";
			boardControls.appendChild(divControls);
			divControls
				.querySelector("#play")
				.addEventListener("click", () => video.play());
			divControls
				.querySelector("#stop")
				.addEventListener("click", () => video.pause());

			document.addEventListener(
				"keyup",
				(event) => {
					if (event.key == spaceKey && video.paused) {
						video.play();
					} else if (event.key == spaceKey && video.played) {
						video.pause();
					}
				},
				false
			);
		} else {
			image.onload = () => {
				var divBis = document.querySelector("#controls");
				lightBox_bloc.removeChild(loader);
				lightBox_bloc.appendChild(image);

				if (divBis) {
					divBis.remove();
				}
			};
		}

		lightBox_bloc.append(mediaTitle);
	}
	menuModal() {
		const div = document.querySelector("#container");
		var messagePlayStop = "Use space key for play/stop vidéo";
		var messageArrow = "Use <--/--> to navigate";
		var messageEscape = "Use ESC for quit player";
		var arr = [messageArrow, messagePlayStop, messageEscape];
		var parragraphe = document.createElement("p");
		parragraphe.tabIndex = 0;
		var i = 0;
		div.setAttribute("id", "theList");

		document.addEventListener("keydown", (e) => {
			div.setAttribute("id", "theList");
			e.preventDefault();
			if (e.key == "Tab") {
				div.style.display = "block";
				if (i >= arr.length) {
					div.style.display = "none";
					return;
				}
				div.style.display = "block";
				parragraphe.innerHTML = arr[i];
				i++;
			}
			if (e.key == "Shift") {
				div.style.display = "block";
				if (i > arr.length - 1) {
					i = 3;
				}
				if (i <= 0) {
					div.style.display = "none";
					i = 0;
				} else {
					i--;
				}
				parragraphe.innerHTML = arr[i];
			}
			div.appendChild(parragraphe);
		});
	}

	/*Permet de fermer la lightbox */
	close(e) {
		e.preventDefault();
		this.element.classList.add("fadeOu");
		window.setTimeout(() => {
			this.element.parentElement.removeChild(this.element);
		}, 500);
		document.removeEventListener("keyup", this.oneKeyUpe);
	}
	/* permet de fermer la lightbox accessible */
	oneKeyUpe(e) {
		if (e.key === "Escape") {
			this.close(e);
		} else if (e.key === "ArrowLeft") {
			this.prev(e);
		} else if (e.key === "ArrowRight") {
			this.next(e);
		}
	}

	prev(e) {
		e.preventDefault();
		this.i = this.gallery.findIndex((media) => media.url === this.url) - 1;
		if (this.i < 0) {
			this.i = this.gallery.length - 1;
		}
		this.loadImage(this.gallery[this.i].url, this.gallery[this.i].title);
	}

	next(e) {
		this.i += 1;
		e.preventDefault();
		this.i = this.gallery.findIndex((media) => media.url === this.url) + 1;
		if (this.i === this.gallery.length) {
			this.i = 0;
		}
		this.loadImage(this.gallery[this.i].url, this.gallery[this.i].title);
	}

	buildDOM() {
		const dom = document.createElement("section");
		dom.classList.add("lightBox");
		dom.innerHTML = `
        <button class="lightBox_close"></button>
        <button class="lightBox_prev"></button>
        <button class="lightBox_next"></button>
		<article class="lightBox_bloc">
			<h2 id="titrePhoto" aria-label="nom de la photo">
			</h2>
        </article> 
        <div id="Containercontrols">
        </div>
        <div id= "container"></div>
        `;
		dom
			.querySelector(".lightBox_close")
			.addEventListener("click", this.close.bind(this));
		dom
			.querySelector(".lightBox_prev")
			.addEventListener("click", this.prev.bind(this));
		dom
			.querySelector(".lightBox_next")
			.addEventListener("click", this.next.bind(this));
		return dom;
	}
}
