const TYPE_TEXT = "text";
const TYPE_EMAIL = "email";

// eslint-disable-next-line no-unused-vars
class DataForm {
	constructor(form, validate = false) {
		this.form = form;
		this.inputs = [];
		this.data = [];
		this.errors = [];
		this.isValid = false;

		this.validationRules = {
			string: {
				minLength: 2,
				maxLength: 50,
				required: true,
			},
			email: {
				// eslint-disable-next-line no-useless-escape
				regex: /^[\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}$/,
				required: true,
			},
		};

		this.initInputs();
		this.initData();

		if (validate) {
			this.validate();
		}
	}

	initInputs() {
		this.inputs = this.form.querySelectorAll("input");
	}

	initData() {
		for (let input of this.inputs) {
			if (input.name && input.type != "submit") {
				this.data.push({
					name: input.name.trim(),
					value: input.value.trim(),
					type: input.type.trim(),
				});
			}
		}
	}

	validate() {
		this.errors = [];

		for (let data of this.data) {
			if (data.type === TYPE_TEXT) {
				this.validateString(data);
			}
			if (data.type === TYPE_EMAIL) {
				this.validateMail(data);
			}
		}

		this.isValid = this.errors.length === 0;
	}

	validateString(dataString) {
		if (this.validationRules.string.required && dataString.value === "") {
			this.pushError(
				dataString.name,
				"Le champ " + dataString.name + " est requis."
			);
		} else if (
			dataString.value.length < this.validationRules.string.minLength
		) {
			this.pushError(
				dataString.name,
				"Le champ " +
          dataString.name +
          " doit contenir au moins " +
          this.validationRules.string.minLength +
          " characteres."
			);
		} else if (
			dataString.value.length > this.validationRules.string.maxLength
		) {
			this.pushError(
				dataString.name,
				"Le champ " +
          dataString.name +
          " doit contenir moins de " +
          this.validationRules.string.minLength +
          " characteres."
			);
		}
	}

	validateMail(dataMail) {
		if (this.validationRules.email.required && dataMail.value === "") {
			this.pushError(
				dataMail.name,
				"Le champ " + dataMail.name + " est requis."
			);
		} else if (!this.validationRules.email.regex.test(dataMail.value)) {
			this.pushError(dataMail.name, "Format mail invalide.");
		}
	}

	pushError(name, content) {
		this.errors.push({
			name: name,
			content: content,
		});
	}
}
