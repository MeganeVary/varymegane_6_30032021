/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
class MediaFactory {
	static createMedia(
		id,
		photographerId,
		title,
		url,
		tags,
		likes,
		date,
		price,
		isVideo = false
	) {
		if (isVideo) {
			return MediaFactory.createVideo(
				id,
				photographerId,
				title,
				url,
				tags,
				likes,
				date,
				price
			);
		} else {
			return MediaFactory.createImage(
				id,
				photographerId,
				title,
				url,
				tags,
				likes,
				date,
				price
			);
		}
	}

	static createImage(id, photographerId, title, url, tags, likes, date,price) {
		// eslint-disable-next-line no-undef
		return new Media(
			id,
			photographerId,
			title,
			url,
			tags,
			likes,
			date,
			image,
			price
		);
	}

	static createVideo(id, photographerId, title, url, tags, likes, date,price) {
		// eslint-disable-next-line no-undef
		return new Media(
			id,
			photographerId,
			title,
			url,
			tags,
			likes,
			date,
			video,
			price
		);
	}
}
