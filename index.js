/* eslint-disable no-undef */
var url = new URLSearchParams(document.location.search.substring(1));
var tagsParam = url.get("tag");
var alertFiltre = document.getElementById("alertFiltre");
if (tagsParam) {
	tagsParam = tagsParam.split(",");
	tagsParam = tagsParam.map((tag) => tag.trim());
	alertFiltre.innerHTML +=
    `
        <p>Vous avez sélectionné tags ` +
    tagsParam +
    `.</p>
        <p>Vous pouvez selectionner plusieurs tags.</p>
        <p>Selectionnez un tag actif pour le supprimer.</p>
    `;
} else {
	tagsParam = [];
}

fetch("FishEyeDataFR.json")
	.then((res) => res.json())
	.then((data) => {
		let photographersContainer = document.getElementById(
			"section_profil_photographe"
		);
		let photographersList = new PhotographersList();

		data.photographers.forEach((dataPhotographer) => {
			let photographer = new Photographer(
				dataPhotographer.id,
				dataPhotographer.name,
				dataPhotographer.city,
				dataPhotographer.country,
				dataPhotographer.tags,
				dataPhotographer.tagline,
				dataPhotographer.price,
				dataPhotographer.portrait
			);
			photographer.setMedias(data.media);

			photographersList.pushPhotographer(photographer);
		});
		let photographers = photographersList.getPhotographersByTagsBis(tagsParam);

		let photographerCards = "";

		photographers.forEach((photographer) => {
			photographerCards += photographer.getHTMLCard();
		});

		photographersContainer.innerHTML = photographerCards;
	})
	.then(() => {
		if (tagsParam) {
			tagsParam.forEach((tag) => {
				let filterButtons = document.querySelectorAll(`[data-filter="${tag}"]`);

				filterButtons.forEach((filterButton) => {
					filterButton.classList.add("btn-active");
				});
			});
		}

		let btnTags = document.getElementsByClassName("button_tag");

		for (let btn of btnTags) {
			btn.addEventListener("click", (e) => {
				e.preventDefault();

				let btnTag = e.currentTarget.dataset.filter;

				if (tagsParam.includes(btnTag)) {
					tagsParam = tagsParam.filter((tag) => {
						if (tag === btnTag) {
							return false;
						}
						return true;
					});
				} else {
					tagsParam.push(btnTag);
				}

				url.set("tag", tagsParam.join(","));

				if (tagsParam.length) {
					window.location.href = "index.html?" + url.toString();
				} else {
					window.location.href = "index.html";
				}
			});
		}
	});
window.addEventListener("scroll", () => {
	let popUp = document.getElementById("mainSkip");
	if (window.scrollY) {
		popUp.style.visibility = "visible";
		setTimeout(function () {
			popUp.style.visibility = "hidden";
		}, 6000);
	}
});

window.addEventListener("keydown",function(e){
	console.log(e.key);
});